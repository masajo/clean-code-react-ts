import React from 'react';
import './Header.scss';

type Props = {
    title: string
}

export const Header = ({title}: Props) => {
    return (
        <div>
            <div className="header">
                <h1>{title} in React TS</h1>
            </div>
        </div>
    )
}
