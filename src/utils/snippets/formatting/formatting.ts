/**
 * CLEAN CODE: Use Formatting
 * You should use ESLint and configure the rules for your team
 */

// * 1. Consistent Capitalization
// * PASCALCASE for class, interface, type & namespaces
// * CAMELCASE for variables, functions & class members 

// BAD:
// const DAYS_IN_WEEK = 7;
// const daysInMonth = 30;

// const songs = ['Back In Black', 'Stairway to Heaven', 'Hey Jude'];
// const Artists = ['ACDC', 'Led Zeppelin', 'The Beatles'];

// function eraseDatabase() {}
// function restore_database() {}

// type animal = { /* ... */ }
// type Container = { /* ... */ }

// GOOD:
// const DAYS_IN_WEEK = 7;
// const DAYS_IN_MONTH = 30;

// const SONGS = ['Back In Black', 'Stairway to Heaven', 'Hey Jude'];
// const ARTISTS = ['ACDC', 'Led Zeppelin', 'The Beatles'];

// function eraseDatabase() {}
// function restoreDatabase() {}

// type Animal = { /* ... */ }
// type Container = { /* ... */ }


// * 2. Functions callers & callees should be closed

// BAD:
// class PerformanceReview {
//     constructor(private readonly employee: Employee) {
//     }
  
//     private lookupPeers() {
//       return db.lookup(this.employee.id, 'peers');
//     }
  
//     private lookupManager() {
//       return db.lookup(this.employee, 'manager');
//     }
  
//     private getPeerReviews() {
//       const peers = this.lookupPeers();
//       // ...
//     }
//     review() {
//         this.getPeerReviews();
//         this.getManagerReview();
//         this.getSelfReview();
//         // ...
//       }
    
//     private getManagerReview() {
//         const manager = this.lookupManager();
//     }

//     private getSelfReview() {
//         // ...
//     }
// }
    
// const review = new PerformanceReview(employee);
// review.review();

// GOOD:

// class PerformanceReview {
//     constructor(private readonly employee: Employee) {
//     }
  
//     review() {
//       this.getPeerReviews();
//       this.getManagerReview();
//       this.getSelfReview();
  
//       // ...
//     }
  
//     private getPeerReviews() {
//       const peers = this.lookupPeers();
//       // ...
//     }
//     private lookupPeers() {
//         return db.lookup(this.employee.id, 'peers');
//       }
    
//     private getManagerReview() {
//         const manager = this.lookupManager();
//     }

//     private lookupManager() {
//         return db.lookup(this.employee, 'manager');
//     }

//     private getSelfReview() {
//     // ...
//     }
// }

// const review = new PerformanceReview(employee);
// review.review();


// * 3. Organize Imports in your files
// Alphabetically organized and grouped by blank lines 
// Get rid of unused imports

// BAD:
// import { TypeDefinition } from '../types/typeDefinition';
// import { AttributeTypes } from '../model/attribute';
// import { ApiCredentials, Adapters } from './common/api/authorization';
// import fs from 'fs';
// import { ConfigPlugin } from './plugins/config/configPlugin';
// import { BindingScopeEnum, Container } from 'inversify';
// import 'reflect-metadata';

// GOOD:
// import 'reflect-metadata';

// import fs from 'fs';
// import { BindingScopeEnum, Container } from 'inversify';

// import { AttributeTypes } from '../model/attribute';
// import { TypeDefinition } from '../types/typeDefinition';

// import { ApiCredentials, Adapters } from './common/api/authorization';
// import { ConfigPlugin } from './plugins/config/configPlugin';

// * 4. Use Alias in Imports

// BAD:
// import { UserService } from '../../../services/UserService';

// GOOD:
// import { UserService } from '@services/UserService';

// Adding in your tsconfig.json
// ...
//   "compilerOptions": {
//     ...
//     "baseUrl": "src",
//     "paths": {
//       "@services": ["services/*"]
//     }
//     ...
//   }
// ...

