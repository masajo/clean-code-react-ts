/**
 * *** CLEAN CODE: Comments
 * Don't comment bad code: ¡Rewrite It!
*/

// * 1. Self explanatory code > Comments
// * Good code documents itself

// BAD:
// Check if subscription is active.
// if (subscription.endDate > Date.now) {  }

// GOOD:
// const isSubscriptionActive = subscription.endDate > Date.now;
// if (isSubscriptionActive) { /* ... */ }

// * 2. Get rid of Commented out code

// BAD:
// type User = {
//     name: string;
//     email: string;
     // age: number; // BAD
     // jobPosition: string; // BAD
//  }

// GOOD:
// type User = {
//     name: string;
//     email: string;
// }

// * 3. Use Version Control, not comments.
// Use **git log** to obtain history of changes

// BAD:
/**
 * 2016-12-20: Removed monads, didn't understand them (RM)
 * 2016-10-01: Improved using special monads (JP)
 * 2016-02-03: Added type-checking (LI)
 * 2015-03-14: Implemented combine (JR)
 */
//  function combine(a: number, b: number): number {
//     return a + b;
//   }

// GOOD:
// function combine(a: number, b: number): number {
//     return a + b;
//   }

// * 4. Avoid Positional Markers
// Get rid of noise and use naming & indentation to estructure your code
// Use collapse/expand blocks to reach Regions of Code

// BAD
// ////////////////////////////////////////////////////////////////////////////////
// // Client class
// ////////////////////////////////////////////////////////////////////////////////
// class Client {
//     id: number;
//     name: string;
//     address: Address;
//     contact: Contact;
  
//     ////////////////////////////////////////////////////////////////////////////////
//     // public methods
//     ////////////////////////////////////////////////////////////////////////////////
//     public describe(): string {
//       // ...
//     }
  
//     ////////////////////////////////////////////////////////////////////////////////
//     // private methods
//     ////////////////////////////////////////////////////////////////////////////////
//     private describeAddress(): string {
//       // ...
//     }
  
//     private describeContact(): string {
//       // ...
//     }
//   };

// GOOD:
// class Client {
//     id: number;
//     name: string;
//     address: Address;
//     contact: Contact;
  
//     public describe(): string {
//       // ...
//     }
  
//     private describeAddress(): string {
//       // ...
//     }
  
//     private describeContact(): string {
//       // ...
//     }
//   };


// * 5. Use TODO comments
// A TODO comment is not an excuse for bad code

// BAD:
// function getActiveSubscriptions(): Promise<Subscription[]> {
     // ensure `dueDate` is indexed.
//     return db.subscriptions.find({ dueDate: { $lte: new Date() } });
//   }

// GOOD:
// function getActiveSubscriptions(): Promise<Subscription[]> {
     // TODO: ensure `dueDate` is indexed.
//     return db.subscriptions.find({ dueDate: { $lte: new Date() } });
//   }

