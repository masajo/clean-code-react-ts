/**
 * CLEAN CODE: CONDITIONAL RENDERING IN REACT
 */

// * 1. When one conditional for rendering
// * Don't use Ternary when condition is false
// * Better use &&

// BAD:
// import React, { useState } from 'react'

// export const ConditionalRenderingWhenTrueBad = () => {
//   const [showConditionalText, setShowConditionalText] = useState(false)

//   const handleClick = () =>
//     setShowConditionalText(showConditionalText => !showConditionalText)

//   return (
//     <div>
//       <button onClick={handleClick}>Toggle the text</button>
//       {showConditionalText ? <p>The condition must be true!</p> : null}
//     </div>
//   )
// }

// GOOD:
// import React, { useState } from 'react'

// export const ConditionalRenderingWhenTrueGood = () => {
//   const [showConditionalText, setShowConditionalText] = useState(false)

//   const handleClick = () =>
//     setShowConditionalText(showConditionalText => !showConditionalText)

//   return (
//     <div>
//       <button onClick={handleClick}>Toggle the text</button>
//       {showConditionalText && <p>The condition must be true!</p>}
//     </div>
//   )
// }


// * 2. When two options for conditional rendering
// * Use Ternary

// BAD:
// import React, { useState } from 'react'

// export const ConditionalRenderingBad = () => {
//   const [showConditionOneText, setShowConditionOneText] = useState(false)

//   const handleClick = () =>
//     setShowConditionOneText(showConditionOneText => !showConditionOneText)

//   return (
//     <div>
//       <button onClick={handleClick}>Toggle the text</button>
//       {showConditionOneText && <p>The condition must be true!</p>}
//       {!showConditionOneText && <p>The condition must be false!</p>}
//     </div>
//   )
// }

// GOOD:
// import React, { useState } from 'react'

// export const ConditionalRenderingGood = () => {
//   const [showConditionOneText, setShowConditionOneText] = useState(false)

//   const handleClick = () =>
//     setShowConditionOneText(showConditionOneText => !showConditionOneText)

//   return (
//     <div>
//       <button onClick={handleClick}>Toggle the text</button>
//       {showConditionOneText ? (
//         <p>The condition must be true!</p>
//       ) : (
//         <p>The condition must be false!</p>
//       )}
//     </div>
//   )
// }


