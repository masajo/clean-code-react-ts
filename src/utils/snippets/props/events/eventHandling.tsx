/**
 * CLEAN CODE: Event Handling in React Components
 */

// * 1. When having an Event Object
// * There's no need to wrap the function and pass the event as a parameter

// BAD:
// import React, { useState } from 'react'

// export const UnnecessaryAnonymousFunctionsBad = () => {
//   const [inputValue, setInputValue] = useState('')

//   const handleChange = e => {
//     setInputValue(e.target.value)
//   }

//   return (
//     <>
//       <label htmlFor="name">Name: </label>
//       <input id="name" value={inputValue} onChange={e => handleChange(e)} />
//     </>
//   )
// }

// GOOD:
// import React, { useState } from 'react'

// export const UnnecessaryAnonymousFunctionsGood = () => {
//   const [inputValue, setInputValue] = useState('')

//   const handleChange = e => {
//     setInputValue(e.target.value)
//   }

//   return (
//     <>
//       <label htmlFor="name">Name: </label>
//       <input id="name" value={inputValue} onChange={handleChange} />
//     </>
//   )
// }

