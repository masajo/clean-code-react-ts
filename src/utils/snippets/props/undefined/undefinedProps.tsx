/**
 * CLEAN CODE: Use of Undefined Props in React Components
 */

// * 1. In React, undefined props are excluded
// * No need to control if it is undefined

// BAD:
// import React from 'react'

// const ButtonOne = ({ handleClick }) => (
//   <button onClick={handleClick || undefined}>Click me</button>
// )

// const ButtonTwo = ({ handleClick }) => {
//   const noop = () => {}

//   return <button onClick={handleClick || noop}>Click me</button>
// }

// export const UndefinedPropsBad = () => (
//   <div>
//     <ButtonOne />
//     <ButtonOne handleClick={() => alert('Clicked!')} />
//     <ButtonTwo />
//     <ButtonTwo handleClick={() => alert('Clicked!')} />
//   </div>
// )

// GOOD:
// import React from 'react'

// const ButtonOne = ({ handleClick }) => (
//   <button onClick={handleClick}>Click me</button>
// )

// export const UndefinedPropsGood = () => (
//   <div>
//     <ButtonOne />
//     <ButtonOne handleClick={() => alert('Clicked!')} />
//   </div>
// )