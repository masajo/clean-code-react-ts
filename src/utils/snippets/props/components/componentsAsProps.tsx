/**
 * CLEAN CODE: Passing Components as Props in React
 */

// * 1. No need to embrace the component if it doesn't receive props 

// BAD:
// import React from 'react'

// const CircleIcon = () => (
//   <svg height="100" width="100">
//     <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
//   </svg>
// )

// const ComponentThatAcceptsAnIcon = ({ IconComponent }) => (
//   <div>
//     <p>Below is the icon component prop I was given:</p>
//     <IconComponent />
//   </div>
// )

// export const UnnecessaryAnonymousFunctionComponentsBad = () => (
//   <ComponentThatAcceptsAnIcon IconComponent={() => <CircleIcon />} />
// )

// GOOD:
// import React from 'react'

// const CircleIcon = () => (
//   <svg height="100" width="100">
//     <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
//   </svg>
// )

// const ComponentThatAcceptsAnIcon = ({ IconComponent }) => (
//   <div>
//     <p>Below is the icon component prop I was given:</p>
//     <IconComponent />
//   </div>
// )

// export const UnnecessaryAnonymousFunctionComponentsGood = () => (
//   <ComponentThatAcceptsAnIcon IconComponent={CircleIcon} />
// )