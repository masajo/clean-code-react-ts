/**
 * CLEAN CODE: STRING props in React
 */

// * 1. No need of Curly Braces when usin literal strings

// BAD:
// import React from 'react'

// const Greeting = ({ personName }) => <p>Hi, {personName}!</p>

// export const StringPropValuesBad = () => (
//   <div>
//     <Greeting personName={"John"} />
//     <Greeting personName={'Matt'} />
//     <Greeting personName={`Paul`} />
//   </div>
// )

// GOOD:
// import React from 'react'

// const Greeting = ({ personName }) => <p>Hi, {personName}!</p>

// export const StringPropValuesGood = () => (
//   <div>
//     <Greeting personName="John" />
//     <Greeting personName="Matt" />
//     <Greeting personName="Paul" />
//   </div>
// )