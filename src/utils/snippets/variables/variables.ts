// Clean Code applied to Variables

// *** 1. Meaningful Names

// BAD CODE:
// export function betweenBad<T>(a1: T, a2: T, a3: T): boolean {
//     return a2 <= a1 && a1 <= a3;
// }

// GOOD CODE:
// export function betweenGood<T>(value: T, left: T, right: T): boolean {
//     return left <= value && value <= right;
// }

// *** 2. Pronounceable Names

// type DtaRcrd102 = {
//     genymdhms: Date;
//     modymdhms: Date;
//     pszqint: number;
// }

// type Customer = {
//     generationTimestamp: Date;
//     modificationTimestamp: Date;
//     recordId: number;
// }


// *** 3. Same Vocabulary

// class User {
     //
// }

// BAD:
// function getUserInfo(): User;
// function getUserDetails(): User;
// function getUserData(): User;

// GOOD:
// function getUser(): User;


// *** 4. Searchable Naming (Magical numbers)

// BAD:
// setTimeout((restart), 8640000);

// GOOD:
// const MILLISECONDS_IN_A_DAY = 24 * 60 * 60 * 1000;
// setTimeout(restart, MILLISECONDS_IN_A_DAY);

// *** 5. Explaining Variables

// declare const users: Map<string, User>;

// BAD:
// for (const keyValue of users) {
   // iterate through users map
// }

// GOOD:
// for (const [id, user] of users) {
    // iterate through users map
// }


// *** 6. Explicit Better than Implicit

// BAD:
// const u = getUser();
// const s = getSubscription();
// const t = charge(u, s);

// GOOD:
// const user = getUser();
// const subscription = getSubscription();
// const transaction = charge(user, subscription);

// *** 7. Context

// BAD:
// type Car = {
//     carMake: string;
//     carModel: string;
//     carColor: string;
// }
  
// function print(car: Car): void {
//     console.log(`${car.carMake} ${car.carModel} (${car.carColor})`);
// }

// GOOD:
// type CarBetter = {
//     make: string;
//     model: string;
//     color: string;
// }
  
// function printBetter(car: CarBetter): void {
//     console.log(`${car.make} ${car.model} (${car.color})`);
// }

// *** 8. Default Arguments

// BAD:
// function loadPages(count?: number) {
//     const loadCount = count !== undefined ? count : 10;
//     // ...
// }

// GOOD:
// function loadPagesGood(count = 10) {
//     // ...
// }

// 9. Use ENUMS

// BAD:
// const GENRE = {
//     ROMANTIC: 'romantic',
//     DRAMA: 'drama',
//     COMEDY: 'comedy',
//     DOCUMENTARY: 'documentary',
// }
  
// projector.configureFilm(GENRE.COMEDY);

// class Projector {
//     // declaration of Projector
//     configureFilm(genre) {
//         switch (genre) {
//         case GENRE.ROMANTIC:
//             // some logic to be executed 
//         }
//     }
// }

// GOOD:

//   enum GENRE {
//     ROMANTIC,
//     DRAMA,
//     COMEDY,
//     DOCUMENTARY,
//   }
  
//   projector.configureFilm(GENRE.COMEDY);
  
//   class Projector {
//     // declaration of Projector
//     configureFilm(genre) {
//       switch (genre) {
//         case GENRE.ROMANTIC:
//           // some logic to be executed 
//       }
//     }
//   }
