// *** 1: Use Setters & Getters

// BAD:
// type BankAccount = {
//     balance: number;
     // ...
// }
  
// const value = 100;
// const account: BankAccount = {
//     balance: 0,
     // ...
// };

// if (value < 0) {
//     throw new Error('Cannot set negative balance.');
// }

// account.balance = value;

// GOOD:
// class BankAccount {
//     private accountBalance: number = 0;
  
//     get balance(): number {
//       return this.accountBalance;
//     }
  
//     set balance(value: number) {
//       if (value < 0) {
//         throw new Error('Cannot set negative balance.');
//       }
  
//       this.accountBalance = value;
//     }
  
     // ...
//   }
  
// const account = new BankAccount();
// account.balance = 100;


// *** 2. Private & Protected

// BAD:
// class Circle {
//     radius: number;
    
//     constructor(radius: number) {
//       this.radius = radius;
//     }
  
//     perimeter() {
//       return 2 * Math.PI * this.radius;
//     }
  
//     surface() {
//       return Math.PI * this.radius * this.radius;
//     }
//   }

// GOOD
// class Circle {
//     constructor(private readonly radius: number) {
//     }

//     perimeter() {
//         return 2 * Math.PI * this.radius;
//     }

//     surface() {
//         return Math.PI * this.radius * this.radius;
//     }
// }

// *** 3. Always prefer immutability

// BAD:
// interface Config {
//     host: string;
//     port: string;
//     db: string;
// }


// GOOD:
// interface Config {
//     readonly host: string;
//     readonly port: string;
//     readonly db: string;
// }

// *** 4. ReadOnly Arrays

// BAD:
// const array: number[] = [ 1, 3, 5 ];
// array = []; // error
// array.push(100); // array will updated

// GOOD:
// const array: ReadonlyArray<number> = [ 1, 3, 5 ];
// array = []; // error
// array.push(100); // error

// *** 5. Prefer Const Assertions 

// BAD:
// const config = {
//     hello: 'world'
// };

// config.hello = 'world'; // value is changed
  
// const array  = [ 1, 3, 5 ];
// array[0] = 10; // value is changed

// writable objects is returned
// function readonlyData(value: number) {
//   return { value };
// }

// const result = readonlyData(100);
// result.value = 200; // value is changed

// GOOD:

// read-only object
// const config = {
//   hello: 'world'
// } as const;

// config.hello = 'world'; // error

// read-only array
// const array  = [ 1, 3, 5 ] as const;
// array[0] = 10; // error

// You can return read-only objects
// function readonlyData(value: number) {
//   return { value } as const;
// }

// const result = readonlyData(100);
// result.value = 200; // error

// *** 6. Type > Interface

// BAD:
// interface EmailConfig {
    // ...
// }
  
// interface DbConfig {
// ...
// }

// interface Config {
// ...
// }

//...

// type Shape = {
// ...
// }

// GOOD:
// type EmailConfig = {
     // ...
// }
  
// type DbConfig = {
   // ...
// }

// type Config  = EmailConfig | DbConfig;

// ...

// interface Shape {
  // ...
// }

// class Circle implements Shape {
  // ...
// }

// class Square implements Shape {
  // ...
// }
