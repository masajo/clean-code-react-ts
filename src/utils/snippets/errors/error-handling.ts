/**
 * CLEAN CODE: Error Handling
 */

// * 1. Always use Errors for rejection

// BAD:
	
// function calculateTotal(items: Item[]): number {
//     throw 'Not implemented.';
// }
   
// function get(): Promise<Item[]> {
//     return Promise.reject('Not implemented.');
// }

// GOOD:
// function calculateTotal(items: Item[]): number {
//     throw new Error('Not implemented.');
// }
  
// function get(): Promise<Item[]> {
//     return Promise.reject(new Error('Not implemented.'));
// }

// or :

// async function get(): Promise<Item[]> {
//     throw new Error('Not implemented.');
// }

// GOOD:
// type Result<R> = { isError: false, value: R };
// type Failure<E> = { isError: true, error: E };
// type Failable<R, E> = Result<R> | Failure<E>;

// function calculateTotal(items: Item[]): Failable<number, 'empty'> {
//   if (items.length === 0) {
//     return { isError: true, error: 'empty' };
//   }

//   // ...
//   return { isError: false, value: 42 };
// }

// * 2. Don't ignore Caught Errors in Code


// VERY BAD:  
// try {
//     functionThatMightThrow();
// } catch (error) {
     // ignore error
// }

// BAD:
// try {
//     functionThatMightThrow();
// } catch (error) {
//     console.log(error);
// }

// GOOD:
// import { logger } from './logging'

// try {
//   functionThatMightThrow();
// } catch (error) {
//   logger.log(error);
// }

// * 3. Don't ignore Rejections in Promises

// BAD:
// getUser()
//   .then((user: User) => {
//     return sendEmail(user.email, 'Welcome!');
//   })
//   .catch((error) => {
//     console.log(error);
//   });

// GOOD:
// import { logger } from './logging'

// getUser()
//   .then((user: User) => {
//     return sendEmail(user.email, 'Welcome!');
//   })
//   .catch((error) => {
//     logger.log(error);
//   });

// GOOD: Same, but using the async/await syntax:

// try {
//   const user = await getUser();
//   await sendEmail(user.email, 'Welcome!');
// } catch (error) {
//   logger.log(error);
// }