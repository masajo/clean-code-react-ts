// Clean Code applied to Variables

// **** 1. Function Arguments

// BAD CODE
// export function createMenuBAD(title: string, body: string, buttonText: string, cancellable: boolean) {
     // ...
// }
  
// createMenuBAD('Foo', 'Bar', 'Baz', true);

// GOOD CODE
// export function createMenuGOOD(options: { title: string, body: string, buttonText: string, cancellable: boolean }) {
    // ...
// }
  
// createMenuGOOD({
//     title: 'Foo',
//     body: 'Bar',
//     buttonText: 'Baz',
//     cancellable: true
// });

// BETTER CODE
// type MenuOptionsBETTER = { 
//   title: string, body: string, buttonText: string, cancellable: boolean 
// };

// function createMenuBETTER(options: MenuOptionsBETTER) {
  // ...
// }

// createMenuBETTER({
//   title: 'Foo',
//   body: 'Bar',
//   buttonText: 'Baz',
//   cancellable: true
// });


// *** 2. Single Action per function

// BAD:
// function emailClients(clients: Client[]) {
//   clients.forEach((client) => {
//     const clientRecord = database.lookup(client);
//     if (clientRecord.isActive()) {
//       email(client);
//     }
//   });
// }

// GOOD:
// function emailClients(clients: Client[]) {
//   clients.filter(isActiveClient).forEach(email);
// }

// function isActiveClient(client: Client) {
//   const clientRecord = database.lookup(client);
//   return clientRecord.isActive();
// }

// *** 3. Naming

// BAD:
// function addToDate(date: Date, month: number): Date {
  // ...
// }

// const date = new Date();

// addToDate(date, 1);

// GOOD
// function addMonthToDate(date: Date, month: number): Date {
  // ...
// }

// const date = new Date();
// addMonthToDate(date, 1);

// *** 4. Abstraction Levels

// BAD:
// function parseCode(code: string) {
//   const REGEXES = [ /* ... */ ];
//   const statements = code.split(' ');
//   const tokens = [];

//   REGEXES.forEach((regex) => {
//     statements.forEach((statement) => {
        // ...
//     });
//   });

// const ast = [];
// tokens.forEach((token) => {
    // lex...
// });

// ast.forEach((node) => {
    // parse...
  // });
// }


// GOOD:
// const REGEXES = [ /* ... */ ];

// function tokenize(code: string): Token[] {
//   const statements = code.split(' ');
//   const tokens: Token[] = [];

//   REGEXES.forEach((regex) => {
//     statements.forEach((statement) => {
//       tokens.push( /* ... */ );
//     });
//   });

//   return tokens;
// }

// function parse(tokens: Token[]): SyntaxTree {
//   const syntaxTree: SyntaxTree[] = [];
//   tokens.forEach((token) => {
//     syntaxTree.push( /* ... */ );
//   });

//   return syntaxTree;
// }

// function parseCode(code: string) {
//   const tokens = tokenize(code);
//   const syntaxTree = parse(tokens);

//   syntaxTree.forEach((node) => {
//     // parse...
//   });
// }

// *** 5. REMOVE DUPLICATION OF CODE

// function showDeveloperList(developers: Developer[]) {
//   developers.forEach((developer) => {
//     const expectedSalary = developer.calculateExpectedSalary();
//     const experience = developer.getExperience();
//     const githubLink = developer.getGithubLink();

//     const data = {
//       expectedSalary,
//       experience,
//       githubLink
//     };

//     render(data);
//   });
// }

// function showManagerList(managers: Manager[]) {
//   managers.forEach((manager) => {
//     const expectedSalary = manager.calculateExpectedSalary();
//     const experience = manager.getExperience();
//     const portfolio = manager.getMBAProjects();

//     const data = {
//       expectedSalary,
//       experience,
//       portfolio
//     };

//     render(data);
//   });
// }

// GOOD:
// class Developer {
//   // ...
//   getExtraDetails() {
//     return {
//       githubLink: this.githubLink,
//     }
//   }
// }

// class Manager {
//   // ...
//   getExtraDetails() {
//     return {
//       portfolio: this.portfolio,
//     }
//   }
// }

// function showEmployeeList(employee: Developer | Manager) {
//   employee.forEach((employee) => {
//     const expectedSalary = employee.calculateExpectedSalary();
//     const experience = employee.getExperience();
//     const extra = employee.getExtraDetails();

//     const data = {
//       expectedSalary,
//       experience,
//       extra,
//     };

//     render(data);
//   });
// }


// *** 6. Default Objects 

// BAD:
// type MenuConfig = { title?: string, body?: string, buttonText?: string, cancellable?: boolean };

// function createMenu(config: MenuConfig) {
//   config.title = config.title || 'Foo';
//   config.body = config.body || 'Bar';
//   config.buttonText = config.buttonText || 'Baz';
//   config.cancellable = config.cancellable !== undefined ? config.cancellable : true;
  // ...
// }

// createMenu({ body: 'Bar' });

// GOOD:
// type MenuConfig = { title?: string, body?: string, buttonText?: string, cancellable?: boolean };

// function createMenu(config: MenuConfig) {
//   const menuConfig = Object.assign({
//     title: 'Foo',
//     body: 'Bar',
//     buttonText: 'Baz',
//     cancellable: true
//   }, config);
    // ...
// }

// createMenu({ body: 'Bar' });

// BETTER:
// type MenuConfig = { title?: string, body?: string, buttonText?: string, cancellable?: boolean };

// function createMenu({ title = 'Foo', body = 'Bar', buttonText = 'Baz', cancellable = true }: MenuConfig) {
  // ...
// }

// createMenu({ body: 'Bar' });

// *** 7. Not use of Options/flags as parameters

// BAD:
// function createFile(name: string, temp: boolean) {
//   if (temp) {
//     fs.create(`./temp/${name}`);
//   } else {
//     fs.create(name);
//   }
// }

// GOOD:
// function createTempFile(name: string) {
//   createFile(`./temp/${name}`);
// }

// function createCompFile(name: string) {
//   createFile(`./components/${name}`);
// }

// function createFile(name: string) {
//   fs.create(name);
// }


// *** 8. Side Effects

// BAD:

// Global variable referenced by following function.
// let name = 'Robert C. Martin';

// function toBase64() {
//   name = btoa(name);
// }
// toBase64();
// console.log(name);

// GOOD:
// const name = 'Robert C. Martin';

// function toBase64(text: string): string {
//   return btoa(text);
// }

// const encodedName = toBase64(name);
// console.log(name);


// *** 10. Referenced Variables and Return values!

// BAD:
// function addItemToCart(cart: CartItem[], item: Item): void {
//   cart.push({ item, date: Date.now() });
// };

// GOOD:
// function addItemToCart(cart: CartItem[], item: Item): CartItem[] {
//   return [...cart, { item, date: Date.now() }];
// };

// *** 11. NO Global Functions that can be changed

// declare global {
//   interface Array<T> {
//     diff(other: T[]): Array<T>;
//   }
// }

// if (!Array.prototype.diff) {
//   Array.prototype.diff = function <T>(other: T[]): T[] {
//     const hash = new Set(other);
//     return this.filter(elem => !hash.has(elem));
//   };
// }

// *** 12. Functional Programming > Imperative Programming

// BAD:
// const contributions = [
//   {
//     name: 'Uncle Bobby',
//     linesOfCode: 500
//   }, {
//     name: 'Suzie Q',
//     linesOfCode: 1500
//   }, {
//     name: 'Jimmy Gosling',
//     linesOfCode: 150
//   }, {
//     name: 'Gracie Hopper',
//     linesOfCode: 1000
//   }
// ];

// let totalOutput = 0;

// for (let i = 0; i < contributions.length; i++) {
//   totalOutput += contributions[i].linesOfCode;
// }


// GOOD:
// const contributions = [
//   {
//     name: 'Uncle Bobby',
//     linesOfCode: 500
//   }, {
//     name: 'Suzie Q',
//     linesOfCode: 1500
//   }, {
//     name: 'Jimmy Gosling',
//     linesOfCode: 150
//   }, {
//     name: 'Gracie Hopper',
//     linesOfCode: 1000
//   }
// ];

// const totalOutput = contributions
//   .reduce((totalLines, output) => totalLines + output.linesOfCode, 0);


// *** 13. Encapsulation of Conditionales

// BAD:
// if (subscription.isTrial || account.balance > 0) {
   // ...
// }

// GOOD:
// function canActivateService(subscription: Subscription, account: Account) {
//   return subscription.isTrial || account.balance > 0;
// }

// if (canActivateService(subscription, account)) {
   // ...
// }

// *** 14. Avoid Negative Conditionals

// BAD:
// function isEmailNotUsed(email: string): boolean {
   // ...
// }

// if (isEmailNotUsed(email)) {
   // ...
// }

// GOOD:
// function isEmailUsed(email: string): boolean {
   // ...
// }

// if (!isEmailUsed(node)) {
//    ...
// }


// *** 15. Avoid Conditionals

// BAD:
// class Airplane {
//   private type: string;
//   // ...

//   getCruisingAltitude() {
//     switch (this.type) {
//       case '777':
//         return this.getMaxAltitude() - this.getPassengerCount();
//       case 'Air Force One':
//         return this.getMaxAltitude();
//       case 'Cessna':
//         return this.getMaxAltitude() - this.getFuelExpenditure();
//       default:
//         throw new Error('Unknown airplane type.');
//     }
//   }

//   private getMaxAltitude(): number {
//      ...
//   }
// }

// GOOD:
// abstract class Airplane {
//   protected getMaxAltitude(): number {
//     // shared logic with subclasses ...
//   }

//    ...
// }

// class Boeing777 extends Airplane {
//    ...
//   getCruisingAltitude() {
//     return this.getMaxAltitude() - this.getPassengerCount();
//   }
// }

// class AirForceOne extends Airplane {
//   ...
//   getCruisingAltitude() {
//     return this.getMaxAltitude();
//   }
// }

// class Cessna extends Airplane {
//    ...
//   getCruisingAltitude() {
//     return this.getMaxAltitude() - this.getFuelExpenditure();
//   }
// }



// *** 19. Avoid Type Checking

// BAD:
// function travelToTexas(vehicle: Bicycle | Car) {
//   if (vehicle instanceof Bicycle) {
//     vehicle.pedal(currentLocation, new Location('texas'));
//   } else if (vehicle instanceof Car) {
//     vehicle.drive(currentLocation, new Location('texas'));
//   }
// }

// GOOD:
// type Vehicle = Bicycle | Car;

// function travelToTexas(vehicle: Vehicle) {
//   vehicle.move(currentLocation, new Location('texas'));
// }

// *** 20. Optimization

// BAD:
// for (let i = 0, len = list.length; i < len; i++) {
   // ...
// }

// GOOD:
// for (let i = 0; i < list.length; i++) {
  // ...
// }

// *** 21: Remove Dead Code

// BAD:
// function oldRequestModule(url: string) {
  // ...
// }

// function requestModule(url: string) {
  // ...
// }

// const req = requestModule;
// inventoryTracker('apples', req, 'www.inventory-awesome.io');


// *** 22: Generators & Iterators

// BAD:
// function fibonacci(n: number): number[] {
//   if (n === 1) return [0];
//   if (n === 2) return [0, 1];

//   const items: number[] = [0, 1];
//   while (items.length < n) {
//     items.push(items[items.length - 2] + items[items.length - 1]);
//   }

//   return items;
// }

// function print(n: number) {
//   fibonacci(n).forEach(fib => console.log(fib));
// }

// Print first 10 Fibonacci numbers.
// print(10);

// GOOD:
// function* fibonacci(): IterableIterator<number> {
//   let [a, b] = [0, 1];

//   while (true) {
//     yield a;
//     [a, b] = [b, a + b];
//   }
// }

// function print(n: number) {
//   let i = 0;
//   for (const fib of fibonacci()) {
//     if (i++ === n) break;  
//     console.log(fib);
//   }  
// }

// Print first 10 Fibonacci numbers.
// print(10);