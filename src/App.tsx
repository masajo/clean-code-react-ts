import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { Header } from './components/Header/Header';
import { GoodExample } from './utils/snippets/conditional rendering/conditional_rendering';

function App() {
  return (
    <div className='App'>
      <Header title='Clean Code' ></Header>
    </div>
  );
}

export default App;
